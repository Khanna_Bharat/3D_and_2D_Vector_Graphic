#include <cr_section_macros.h>
#include <NXP/crp.h>

__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

#include "LPC17xx.h"                        /* LPC13xx definitions */
#include "ssp.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "font.h"
#include "extint.h"
#include<math.h>

#define pi 3.14
/* Be careful with the port number and location number, because
some of the location may not exist in that port. */
#define PORT_NUM			1
#define LOCATION_NUM		0

#define pgm_read_byte(addr) (*(const unsigned char *)(addr))


//#define OFFSETX 60
#define OFFSETX 64

// offset for screen wont change unless
//#define OFFSETY 60
#define OFFSETY 64
// i use different screen! so its kinda fixed
//#define OFFSETZ 23
#define OFFSETZ 64
//#define OFFSETZ 22

uint8_t src_addr[SSP_BUFSIZE];
uint8_t dest_addr[SSP_BUFSIZE];
int colstart = 0;
int rowstart = 0;


#define ST7735_TFTWIDTH  127
#define ST7735_TFTHEIGHT 159
#define ST7735_CASET   0x2A
#define ST7735_RASET   0x2B
#define ST7735_RAMWR   0x2C
#define swap(x, y) { x = x + y; y = x - y; x = x - y; }
#define GREEN 0x00FF00
#define BLACK  0x000000
#define CYAN 0x07FF
#define BLUE  0x0000FF
#define WHITE  0xFFFFFF
#define RED  0xFF00   // originally it's FF0000
#define SILVER 0xFFA0FF
#define ORANGE 0xFF8000


int _height = ST7735_TFTHEIGHT;
int _width = ST7735_TFTWIDTH;
int cursor_x = 0, cursor_y = 0;
uint16_t textcolor = RED, textbgcolor= GREEN;
float textsize = 2;
int wrap = 1;



void spiwrite(uint8_t c)
{
	int portnum = 1;

	src_addr[0] = c;
	SSP_SSELToggle( portnum, 0 );
	SSPSend( portnum, (uint8_t *)src_addr, 1 );
	SSP_SSELToggle( portnum, 1 );

}
void writecommand(uint8_t c) {
	LPC_GPIO0->FIOCLR |= (0x1<<21);
	spiwrite(c);
}
void writedata(uint8_t c) {

	LPC_GPIO0->FIOSET |= (0x1<<21);
	spiwrite(c);
}
void writeword(uint16_t c) {

	uint8_t d;

	d = c >> 8;
	writedata(d);
	d = c & 0xFF;
	writedata(d);
}
void write888(uint32_t color, uint32_t repeat) {
	uint8_t red, green, blue;
	int i;
	red = (color >> 16);
	green = (color >> 8) & 0xFF;
	blue = color & 0xFF;
	for (i = 0; i< repeat; i++) {
		writedata(red);
		writedata(green);
		writedata(blue);
	}
}

void setAddrWindow(uint16_t x0, uint16_t y0, uint16_t x1,
					uint16_t y1) {

	  writecommand(ST7735_CASET);
	  writeword(x0);
	  writeword(x1);
	  writecommand(ST7735_RASET);
	  writeword(y0);
	  writeword(y1);

}

void drawPixel(int16_t x, int16_t y, uint16_t color) {
    if((x < 0) ||(x >= _width) || (y < 0) || (y >= _height)) return;

    setAddrWindow(x,y,x+1,y+1);
    writecommand(ST7735_RAMWR);

    write888(color, 1);
}

void fillrect(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint32_t color)
{
	int16_t width, height;
	width = x1-x0+1;
	height = y1-y0+1;
	setAddrWindow(x0,y0,x1,y1);
	writecommand(ST7735_RAMWR);
	write888(color,width*height);
}

void lcddelay(int ms)
{
	int count = 24000;
	int i;

	for ( i = count*ms; i--; i > 0);
}


void lcd_init()
{
/*
 * portnum 	= 0 ;
 * cs 		= p0.16 / p0.6 ?
 * rs		= p0.21
 * rst		= p0.22
 */
	uint32_t portnum = 1;
	int i;
	printf(" in lcd_init\n");
	/* Notice the hack, for portnum 0 p0.16 is used */
	if ( portnum == 0 )
	  {
		LPC_GPIO0->FIODIR |= (0x1<<16);		/* SSP1, P0.16 defined as Outputs */
	  }
	  else
	  {
		LPC_GPIO0->FIODIR |= (0x1<<6);		/* SSP0 P0.6 defined as Outputs */
	  }
	/* Set rs(dc) and rst as outputs */
	LPC_GPIO0->FIODIR |= (0x1<<21);		/* rs/dc P0.21 defined as Outputs */
	LPC_GPIO0->FIODIR |= (0x1<<22);		/* rst P0.22 defined as Outputs */


	/* Reset sequence */
	LPC_GPIO0->FIOSET |= (0x1<<22);
	LPC_GPIO0->FIOCLR |= (0x1<<22);
	LPC_GPIO0->FIOSET |= (0x1<<22);
	 for ( i = 0; i < SSP_BUFSIZE; i++ )	/* Init RD and WR buffer */
	    {
	  	  src_addr[i] = 0;
	  	  dest_addr[i] = 0;
	    }

	 /* do we need Sw reset (cmd 0x01) ? */


	 SSP_SSELToggle( portnum, 0 );
	 src_addr[0] = 0x11;									// Sleep out
	 SSPSend( portnum, (uint8_t *)src_addr, 1 );
	 SSP_SSELToggle( portnum, 1 );
	 SSP_SSELToggle( portnum, 0 );
	 src_addr[0] = 0x29;									// Disp On
	 SSPSend( portnum, (uint8_t *)src_addr, 1 );
	 SSP_SSELToggle( portnum, 1 );
}

//Draw line function
 void drawline(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint32_t color)
 {
 	int16_t slope = abs(y1 - y0) > abs(x1 - x0);
 	if (slope)
 	{
 	swap(x0, y0);
 	swap(x1, y1);
 	}

 	if (x0 > x1)
 	{
 	swap(x0, x1);
 	swap(y0, y1);
 	}

 	int16_t dx, dy;
 	dx = x1 - x0;
 	dy = abs(y1 - y0);

 	int16_t err = dx / 2;
 	int16_t ystep;

 	if (y0 < y1)
 	{
 	ystep = 1;
 	} else
 	{
 	ystep = -1;
 	}

 	for (; x0<=x1; x0++)
 	{
 	if (slope)
 	{
 	drawPixel(y0, x0, color);
 	} else
 	{
 	drawPixel(x0, y0, color);
 	}
 	err -= dy;
 	if (err < 0)
 	{
 	y0 += ystep;
 	err += dx;
 	}
 	}
 }

 float xcor[8]={64,-39,-39,64,64,-39,-39,64}; // x data for shape vertex
 float ycor[8]={64,64,-39,-39,64,64,-39,-39}; // y data for shape vertex
  float zcor[8]={-62.5,-62.5,-62.5,-62.5,62.5,62.5,62.5,62.5}; // z data for shape vertex
  const int seqx[12]={0,1,2,3,4,5,6,7,0,1,2,3}; // start vertex for lines
  const int seqy[12]={1,2,3,0,5,6,7,4,4,5,6,7}; // end vertex for lines
  unsigned int startx,starty,endx,endy; // define global variables for calling graphics subroutines

  float Xe = 72 + OFFSETX ;
  float Ye = 72 + OFFSETY;
  float Ze = 72 + OFFSETZ;

  double x_prime[12];
  double y_prime[12];
  double z_prime[12];

void cube()
{
	    double newx[12];
		double newy[12];
		int i;
		double phita,phi;
		int vertex;
		int D = 150 ;  // try D=  150 and 90 for shadow
		float a= Xe;
		float b = Ze;
		Xe = pow(Xe, 2);
		Ye = pow(Ye, 2);
		Ze = pow(Ze, 2);
		phita = acosf( a/( sqrt(Xe + Ye) ) );
		phi = acosf( b/( sqrt( Xe + Ye + Ze ) ) );

			double rho =  sqrt( Xe + Ye + Ze );
			//phita = (pi/4);
			//phi = (3*pi/2);

		for(i=0 ; i < 12; i++ )					//to calculate new point that is camera
		{
		x_prime[i] = ( ( (-sin(phita) ) * xcor[i] ) + ( cos(phita) * ycor[i] )	);
		y_prime[i] = ( ( (-cos(phita) ) * ( cos(phi) ) * xcor[i] ) - ( sin(phita) * ( cos(phi) ) * ycor[i] ) + (sin(phi) * zcor[i])	);
		z_prime[i] = ( ( (-cos(phita) ) * ( sin(phi) ) * xcor[i] ) - ( cos(phita) * ( sin(phi) ) * ycor[i] ) - ((cos(phi) * zcor[i])) + rho	);
		z_prime[i] = z_prime[i] + OFFSETZ;
		newx[i] =  ( x_prime[i] * D  * ( 1/ ( z_prime[i] ) ) );
		newx[i] = newx[i] +  OFFSETX;
		newy[i] =  ( y_prime[i] * D  * ( 1/ ( z_prime[i] ) ) );
		newy[i] =  newy[i] + OFFSETY;;
		}

for (i = 0; i < 1; i++)

	    {
				vertex = seqx[i];
				startx = (unsigned int)newx[vertex];
				starty = (unsigned int)newy[vertex];
				vertex = seqy[i];
				endx = (unsigned int)newx[vertex];
				endy = (unsigned int)newy[vertex];
				drawline(startx, starty, endx, endy, CYAN);
	    }
			for (i = 3; i < 4; i++)

	    {
				vertex = seqx[i];
				startx = (unsigned int)newx[vertex];
				starty = (unsigned int)newy[vertex];
				vertex = seqy[i];
				endx = (unsigned int)newx[vertex];
				endy = (unsigned int)newy[vertex];
				drawline(startx, starty, endx, endy, CYAN);
	    }

			for (i = 4; i < 8; i++)

	    {
				vertex = seqx[i];
				startx = (unsigned int)newx[vertex];
				starty = (unsigned int)newy[vertex];
				vertex = seqy[i];
				endx = (unsigned int)newx[vertex];
				endy = (unsigned int)newy[vertex];
				drawline(startx, starty, endx, endy, CYAN);
	    }
			for (i = 8; i < 10; i++)

				    {
							vertex = seqx[i];
							startx = (unsigned int)newx[vertex];
							starty = (unsigned int)newy[vertex];
							vertex = seqy[i];
							endx = (unsigned int)newx[vertex];
							endy = (unsigned int)newy[vertex];
							drawline(startx, starty, endx, endy, CYAN);
				    }
			for (i = 11; i < 12; i++)

					    {
								vertex = seqx[i];
								startx = (unsigned int)newx[vertex];
								starty = (unsigned int)newy[vertex];
								vertex = seqy[i];
								endx = (unsigned int)newx[vertex];
								endy = (unsigned int)newy[vertex];
								drawline(startx, starty, endx, endy, CYAN);
					    }
}

void cube_shadow()
{
	    double newx[12];
		double newy[12];
		int i;
		double phita,phi;
		int vertex;
		int D = 90 ;  // try D=  150 and 90 for shadow
		float a= Xe;
		float b = Ze;
		Xe = pow(Xe, 2);
		Ye = pow(Ye, 2);
		Ze = pow(Ze, 2);
		phita = acosf( a/( sqrt(Xe + Ye) ) );
		phi = acosf( b/( sqrt( Xe + Ye + Ze ) ) );

			double rho =  sqrt( Xe + Ye + Ze );
			//phita = (pi/4);
			//phi = (3*pi/2);

		for(i=0 ; i < 12; i++ )					//to calculate new point that is camera
		{
		x_prime[i] = ( ( (-sin(phita) ) * xcor[i] ) + ( cos(phita) * ycor[i] )	);
		y_prime[i] = ( ( (-cos(phita) ) * ( cos(phi) ) * xcor[i] ) - ( sin(phita) * ( cos(phi) ) * ycor[i] ) + (sin(phi) * zcor[i])	);
		z_prime[i] = ( ( (-cos(phita) ) * ( sin(phi) ) * xcor[i] ) - ( cos(phita) * ( sin(phi) ) * ycor[i] ) - ((cos(phi) * zcor[i])) + rho	);
		z_prime[i] = z_prime[i] + OFFSETZ;
		newx[i] =  ( x_prime[i] * D  * ( 1/ ( z_prime[i] ) ) );
		newx[i] = newx[i] +  OFFSETX;
		newy[i] =  ( y_prime[i] * D  * ( 1/ ( z_prime[i] ) ) );
		newy[i] =  newy[i] + OFFSETY;;
		}

for (i = 0; i < 1; i++)

	    {
				vertex = seqx[i];
				startx = (unsigned int)newx[vertex];
				starty = (unsigned int)newy[vertex];
				vertex = seqy[i];
				endx = (unsigned int)newx[vertex];
				endy = (unsigned int)newy[vertex];
				drawline(startx, starty, endx, endy, CYAN);
	    }
			for (i = 3; i < 4; i++)

	    {
				vertex = seqx[i];
				startx = (unsigned int)newx[vertex];
				starty = (unsigned int)newy[vertex];
				vertex = seqy[i];
				endx = (unsigned int)newx[vertex];
				endy = (unsigned int)newy[vertex];
				drawline(startx, starty, endx, endy, CYAN);
	    }

			for (i = 4; i < 8; i++)

	    {
				vertex = seqx[i];
				startx = (unsigned int)newx[vertex];
				starty = (unsigned int)newy[vertex];
				vertex = seqy[i];
				endx = (unsigned int)newx[vertex];
				endy = (unsigned int)newy[vertex];
				drawline(startx, starty, endx, endy, CYAN);
	    }
			for (i = 8; i < 10; i++)

				    {
							vertex = seqx[i];
							startx = (unsigned int)newx[vertex];
							starty = (unsigned int)newy[vertex];
							vertex = seqy[i];
							endx = (unsigned int)newx[vertex];
							endy = (unsigned int)newy[vertex];
							drawline(startx, starty, endx, endy, CYAN);
				    }
			for (i = 11; i < 12; i++)

					    {
								vertex = seqx[i];
								startx = (unsigned int)newx[vertex];
								starty = (unsigned int)newy[vertex];
								vertex = seqy[i];
								endx = (unsigned int)newx[vertex];
								endy = (unsigned int)newy[vertex];
								drawline(startx, starty, endx, endy, CYAN);
					    }


			{
			/*
				double xs = 1100;
				double ys = 1110;
				double zs = 110 + OFFSETZ;
			    double newx[12];
				double newy[12];
				int D=90;
				*/
				double xs = 10 + OFFSETX;
				double ys = 10 + OFFSETY;
				double zs = 1 + OFFSETZ;

			    double newx[12];
				double newy[12];
				int D=110;

				int vertex;
			int i;
			for(i = 0 ; i <=8 ; i++ )
			{
			x_prime[i] = ( xcor[i] - ( ( zcor[i] / (zs - zcor[i] ) ) * ( xs - xcor[i] ) ) );
			y_prime[i] = ( ycor[i] - ( ( zcor[i] / (zs - zcor[i] ) ) * ( ys - ycor[i] ) ) );
			z_prime[i] = ( zcor[i] - ( ( zcor[i] / (zs - zcor[i] ) ) * ( zs - zcor[i] ) ) );
			z_prime[i] = z_prime[i] + OFFSETZ;
			newx[i] =  ( x_prime[i] * D  * ( 1/ ( z_prime[i] ) ) );
			newx[i] = newx[i] +  OFFSETX;
			newy[i] =  ( y_prime[i] * D  * ( 1/ ( z_prime[i] ) ) );
			newy[i] =  newy[i] + OFFSETY;;
			}
			for (i = 0; i < 12; i++)

			{
					vertex = seqx[i];
					startx = (unsigned int)newx[vertex];
					starty = (unsigned int)newy[vertex];
					vertex = seqy[i];
					endx = (unsigned int)newx[vertex];
					endy = (unsigned int)newy[vertex];
					drawline(startx, starty, endx, endy, CYAN);
			}
			}
}



void screensaver()

{

	float Zcor[8]={80,80,80,10,10,10,10,80}; // x data for shape vertex
	float Xcor[8]={20,90,90,90,90,20,20,20}; // y data for shape vertex
	float Ycor[8]={100,100,100,100,100,100,100,100}; // z data for shape vertex

	int seqX[8]={0,2,4,6,8,10,12,14}; // start vertex for lines
	  int seqY[8]={1,3,5,7,9,11,13,15}; // end vertex for lines
		//  unsigned int startX,startY,endX,endY; // define global variables for calling graphics subroutines
		  double startX,startY,endX,endY;
		double  X_prime[8], Y_prime[8], Z_prime[8];
		  float Xe_s =72 + OFFSETX;
		  float Ye_s = 72 + OFFSETY;
		  float Ze_s = 72 + OFFSETZ;
		    double newX[8];
			double newY[8];
			int I;
			double phita_s,phi_s;
			int vertex_s;
			int D_s = 100 ;  // try D= 2,4,-2,-4
			float A= Xe_s;
			float B = Ze_s;
			Xe_s = pow(Xe_s, 2);
			Ye_s = pow(Ye_s, 2);
			Ze_s = pow(Ze_s, 2);
			phita_s = acosf( A/( sqrt(Xe_s + Ye_s) ) );
			phi_s = acosf( B/( sqrt( Xe_s + Ye_s + Ze_s ) ) );

				double rho_s =  sqrt( Xe_s + Ye_s + Ze_s );
				//phita = (pi/4);
				//phi = (3*pi/2);

			for(I=0 ; I <8; I++ )					//to calculate new point that is camera
			{
			X_prime[I] = ( ( (-sin(phita_s) ) * Xcor[I] ) + ( cos(phita_s) * Ycor[I] )	);
			Y_prime[I] = ( ( (-cos(phita_s) ) * ( cos(phi_s) ) * Xcor[I] ) - ( sin(phita_s) * ( cos(phi_s) ) * Ycor[I] ) + (sin(phi_s) * Zcor[I])	);
			Z_prime[I] = ( ( (-cos(phita_s) ) * ( sin(phi_s) ) * Xcor[I] ) - ( cos(phita_s) * ( sin(phi_s) ) * Ycor[I] ) - ((cos(phi_s) * Zcor[I])) + rho_s	);
			Z_prime[I] = Z_prime[I] + OFFSETZ;
			newX[I] =  ( X_prime[I] * D_s  * ( 1/ ( Z_prime[I] ) ) );
			newX[I] = newX[I] +  OFFSETX;
			newY[I] =  ( Y_prime[I] * D_s  * ( 1/ ( Z_prime[I] ) ) );
			newY[I] =  newY[I] + OFFSETY;;
			}
			for (I = 0; I < 4; I++)

		    {
					vertex_s = seqX[I];
					startX = (unsigned int)newX[vertex_s];
					startY = (unsigned int)newY[vertex_s];
					vertex_s = seqY[I];
					endX = (unsigned int)newX[vertex_s];
					endY = (unsigned int)newY[vertex_s];
					drawline(startX, startY, endX, endY, BLACK);
		    }
					  int p;
					  double u0,u1,u2,u3;
					  double v0,v1,v2,v3;
				for(p=0;p<6;p++)
				{
				  	 u0=(0.5*(newX[2]-newX[0]))+newX[0];
				  	 v0=(0.5*(newY[2]-newY[0]))+newY[0];

				  	 u1=(0.5*(newX[4]-newX[2]))+newX[2];
				  	 v1=(0.5*(newY[4]-newY[2]))+newY[2];

				  	 u2=(0.5*(newX[6]-newX[4]))+newX[4];
				  	 v2=(0.5*(newY[6]-newY[4]))+newY[4];

				  	 u3=(0.5*(newX[0]-newX[6]))+newX[6];
				  	 v3=(0.5*(newY[0]-newY[6]))+newY[6];

				  	 drawline(u0,v0,u1,v1,BLACK);
				  	 drawline(u1,v1,u2,v2,BLACK);
				  	 drawline(u2,v2,u3,v3,BLACK);
				  	 drawline(u3,v3,u0,v0,BLACK);

				  	newX[0]=u2;
				  	newX[2]=u3;
				  	newX[4]=u0;
				  	newX[6]=u1;
				  	newY[2]=v3;
				  	newY[4]=v0;
				  	newY[6]=v1;
				  	newY[0]=v2;
				}



}

void name()

{

	float Xcor[24]={80,10,10,10,10,40,40,5,5,20, 20,50, 50,65,65,80, 80,60, 60,80, 80,80}; // x data for shape vertex
	float Ycor[24]={90,90,90,70,70,70, 70,10,10,10, 10,65, 60,10 ,10,10, 10,70, 70,70, 70,90}; // y data for shape vertex
	float Zcor[24]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100}; // z data for shape vertex

	int seqX[12]={0,2,4,6,8,10,12,14,16,18,20,22}; // start vertex for lines
	  int seqY[12]={1,3,5,7,9,11,13,15,17,19,21,23}; // end vertex for lines
		//  unsigned int startX,startY,endX,endY; // define global variables for calling graphics subroutines
		  double startX,startY,endX,endY;
		double  X_prime[24], Y_prime[24], Z_prime[24];
		  float Xe_s =72 + OFFSETX;
		  float Ye_s = 72 + OFFSETY;
		  float Ze_s = 72 + OFFSETZ;
		    double newX[24];
			double newY[24];
			int I;
			double phita_s,phi_s;
			int vertex_s;
			int D_s = 100 ;  // try D= 2,4,-2,-4
			float A= Xe_s;
			float B = Ze_s;
			Xe_s = pow(Xe_s, 2);
			Ye_s = pow(Ye_s, 2);
			Ze_s = pow(Ze_s, 2);
			phita_s = acosf( A/( sqrt(Xe_s + Ye_s) ) );
			phi_s = acosf( B/( sqrt( Xe_s + Ye_s + Ze_s ) ) );

				double rho_s =  sqrt( Xe_s + Ye_s + Ze_s );
				//phita = (pi/4);
				//phi = (3*pi/2);

			for(I=0 ; I <22; I++ )					//to calculate new point that is camera
			{
			X_prime[I] = ( ( (-sin(phita_s) ) * Xcor[I] ) + ( cos(phita_s) * Ycor[I] )	);
			Y_prime[I] = ( ( (-cos(phita_s) ) * ( cos(phi_s) ) * Xcor[I] ) - ( sin(phita_s) * ( cos(phi_s) ) * Ycor[I] ) + (sin(phi_s) * Zcor[I])	);
			Z_prime[I] = ( ( (-cos(phita_s) ) * ( sin(phi_s) ) * Xcor[I] ) - ( cos(phita_s) * ( sin(phi_s) ) * Ycor[I] ) - ((cos(phi_s) * Zcor[I])) + rho_s	);
			Z_prime[I] = Z_prime[I] + OFFSETZ;
			newX[I] =  ( X_prime[I] * D_s  * ( 1/ ( Z_prime[I] ) ) );
			newX[I] = newX[I] +  OFFSETX;
			newY[I] =  ( Y_prime[I] * D_s  * ( 1/ ( Z_prime[I] ) ) );
			newY[I] =  newY[I] + OFFSETY;;
			}

			for (I = 0; I <11; I++)

		    {
					vertex_s = seqX[I];
					startX = (unsigned int)newX[vertex_s];
					startY = (unsigned int)newY[vertex_s];
					vertex_s = seqY[I];
					endX = (unsigned int)newX[vertex_s];
					endY = (unsigned int)newY[vertex_s];
					drawline(startX, startY, endX, endY, BLACK);
		    }
}

void texasmap()

{

	float Ycor[24]={40,65,65,65,65,90,90,85,85,60,60,40,40,10,10,10,10,40,40,40, 50,50, 50,10}; // x data for shape vertex
	float Zcor[24]={70,70,70,30,30,30,30,10,10,10,10,-50,-50,12,12,55,55,55,55,70, 70,-28, 30,30}; // y data for shape vertex
	float Xcor[24]={100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100}; // z data for shape vertex
	int seqX[12]={0,2,4,6,8,10,12,14,16,18,20,22}; // start vertex for lines
	  int seqY[12]={1,3,5,7,9,11,13,15,17,19,21,23}; // end vertex for lines
		  unsigned int startX,startY,endX,endY; // define global variables for calling graphics subroutines
		double  X_prime[24], Y_prime[24], Z_prime[24];
		  float Xe_s =72 + OFFSETX;
		  float Ye_s = 72 + OFFSETY;
		  float Ze_s = 72 + OFFSETZ;
		    double newX[24];
			double newY[24];
			int I;
			double phita_s,phi_s;
			int vertex_s;
			int D_s = 100 ;  // try D= 2,4,-2,-4
			float A= Xe_s;
			float B = Ze_s;
			Xe_s = pow(Xe_s, 2);
			Ye_s = pow(Ye_s, 2);
			Ze_s = pow(Ze_s, 2);
			phita_s = acosf( A/( sqrt(Xe_s + Ye_s) ) );
			phi_s = acosf( B/( sqrt( Xe_s + Ye_s + Ze_s ) ) );

				double rho_s =  sqrt( Xe_s + Ye_s + Ze_s );
				//phita = (pi/4);
				//phi = (3*pi/2);

			for(I=0 ; I <24; I++ )					//to calculate new point that is camera
			{
			X_prime[I] = ( ( (-sin(phita_s) ) * Xcor[I] ) + ( cos(phita_s) * Ycor[I] )	);
			Y_prime[I] = ( ( (-cos(phita_s) ) * ( cos(phi_s) ) * Xcor[I] ) - ( sin(phita_s) * ( cos(phi_s) ) * Ycor[I] ) + (sin(phi_s) * Zcor[I])	);
			Z_prime[I] = ( ( (-cos(phita_s) ) * ( sin(phi_s) ) * Xcor[I] ) - ( cos(phita_s) * ( sin(phi_s) ) * Ycor[I] ) - ((cos(phi_s) * Zcor[I])) + rho_s	);
			Z_prime[I] = Z_prime[I] + OFFSETZ;
			newX[I] =  ( X_prime[I] * D_s  * ( 1/ ( Z_prime[I] ) ) );
			newX[I] = newX[I] +  OFFSETX;
			newY[I] =  ( Y_prime[I] * D_s  * ( 1/ ( Z_prime[I] ) ) );
			newY[I] =  newY[I] + OFFSETY;;
			}

			for (I = 0; I < 12; I++)

		    {
					vertex_s = seqX[I];
					startX = (unsigned int)newX[vertex_s];
					startY = (unsigned int)newY[vertex_s];
					vertex_s = seqY[I];
					endX = (unsigned int)newX[vertex_s];
					endY = (unsigned int)newY[vertex_s];
					drawline(startX, startY, endX, endY, CYAN);
		    }
}

void shadow()
{
/*
	double xs = 1100;
	double ys = 1110;
	double zs = 110 + OFFSETZ;
    double newx[12];
	double newy[12];
	int D=90;
	*/
	double xs = 3 + OFFSETX;
	double ys = 3 + OFFSETY;
	double zs = 1.2 + OFFSETZ;

    double newx[12];
	double newy[12];
	int D=155;

	int vertex;
int i;
for(i = 0 ; i <=8 ; i++ )
{
x_prime[i] = ( xcor[i] - ( ( zcor[i] / (zs - zcor[i] ) ) * ( xs - xcor[i] ) ) );
y_prime[i] = ( ycor[i] - ( ( zcor[i] / (zs - zcor[i] ) ) * ( ys - ycor[i] ) ) );
z_prime[i] = ( zcor[i] - ( ( zcor[i] / (zs - zcor[i] ) ) * ( zs - zcor[i] ) ) );
z_prime[i] = z_prime[i] + OFFSETZ;
newx[i] =  ( x_prime[i] * D  * ( 1/ ( z_prime[i] ) ) );
newx[i] = newx[i] +  OFFSETX;
newy[i] =  ( y_prime[i] * D  * ( 1/ ( z_prime[i] ) ) );
newy[i] =  newy[i] + OFFSETY;;
}
for (i = 0; i < 4; i++)				// back
//	for (i = 4; i < 8; i++)				//front
//			for (i = 0; i < 12; i++)	//with projection

{
		vertex = seqx[i];
		startx = (unsigned int)newx[vertex];
		starty = (unsigned int)newy[vertex];
		vertex = seqy[i];
		endx = (unsigned int)newx[vertex];
		endy = (unsigned int)newy[vertex];
		drawline(startx, starty, endx, endy, CYAN);
}
}

void drawEachBranchline(int16_t x0, int16_t y0, int16_t x1, int16_t y1,uint16_t colour) {
    int16_t slopeOfLine = abs(y1 - y0) > abs(x1 - x0);
    if (slopeOfLine) {
        swap(x0, y0);
        swap(x1, y1);
    }

    if (x0 > x1) {
        swap(x0, x1);
        swap(y0, y1);
    }

    int16_t dx, dy;
    dx = x1 - x0;
    dy = abs(y1 - y0);

    int16_t incrementalChange = dx / 2;
    int16_t yStepIncrement;

    if (y0 < y1) {
    	yStepIncrement = 1;
    } else {
    	yStepIncrement = -1;
    }

    for (; x0<=x1; x0++) {
        if (slopeOfLine) {
            drawPixel(y0, x0, colour);
        } else {
            drawPixel(x0, y0, colour);
        }
        incrementalChange -= dy;
        if (incrementalChange < 0) {
            y0 += yStepIncrement;
            incrementalChange += dx;
        }
    }
}



void drawtreeBranches(int itterationLevel, double x, double y, double aFactor, double branchRadius) {
	 if (itterationLevel==0)
	 return;

	 double x1=x+branchRadius*cos(aFactor);
	 double y1=y+branchRadius*sin(aFactor);

	 drawEachBranchline(x,y,x1,y1,WHITE);


	 drawtreeBranches(itterationLevel-1,x1,y1,aFactor-0.52,branchRadius*0.6);
	 drawtreeBranches(itterationLevel-1,x1,y1,aFactor+0.52,branchRadius*0.6);

 }

 /******************************************************************************
 **   Main Function  main()
 ******************************************************************************/
 int main (void)
 {
    uint32_t portnum = PORT_NUM;
    portnum = 1 ; // For LCD use 0
    if ( portnum == 0 )
 	SSP0Init();			// initialize SSP port
    else if ( portnum == 1 )
 	SSP1Init();
    lcd_init();
	fillrect(0, 0, ST7735_TFTWIDTH, ST7735_TFTHEIGHT, WHITE);  // fill upper part of screen with the rectangle
	cube();
	//texasmap();
	//screensaver();
	//name();
	//cube_shadow();
	//shadow();

/*
	  EINTInit();
	  int itterationLevel = 10; // number of branches
	  int leftPixelTree = 42;       // Tree on right size
	  int centerPixelTree = 46;
	  int rightPixelTree = 53;      // Tree on left size
	  int branchRadiusCenter = 5;
	  int branchRadiusright= 9;    // Radius of tree on right side
	  int branchRadiusleft= 7;             // Radius of tree on left side
	  int origin = 32 ;
	  double aFactor = pi/2;
	  drawtreeBranches(itterationLevel, leftPixelTree, 46, aFactor, branchRadiusright); // making tree right side
	  drawtreeBranches(itterationLevel, centerPixelTree, 24, aFactor, branchRadiusCenter); // making tree at center
	  drawtreeBranches(itterationLevel, rightPixelTree, origin, aFactor, branchRadiusleft); // making tree left side
	  drawtreeBranches(5, 46, 56, aFactor, 3); // making tree left side
	  drawtreeBranches(9, 42, 35, aFactor, 7); // making tree left side
*/
	return 0;
 }

 /******************************************************************************
 **                            End Of File
 ******************************************************************************/
